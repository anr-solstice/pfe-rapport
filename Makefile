PDF=pdflatex -shell-escape
BIB=bibtex
RM=rm -rfv
SUBDIRS="rapport"

all:
	for dir in $(SUBDIRS); do \
		$(MAKE) -C $$dir; \
	done
	find . \( -name pdf -prune \) -o -name "_*.pdf" -exec cp -p {} "pdf/" ";" ;
	cd pdf; make 

clean:
	${RM} *.aux *.log *.toc *.out *.pyg *.glo *.ist *.bbl *.blg *.gz *.ind *.idx *.ilg; \
	for dir in $(SUBDIRS); do \
		$(MAKE) clean -C $$dir;  \
	done
	cd pdf; make clean


veryclean: clean	
	${RM} *.dvi *.pdf; \
	for dir in $(SUBDIRS); do \
		$(MAKE) mrproper -C $$dir; \
	done

mrproper: veryclean
	cd pdf; make mrproper

